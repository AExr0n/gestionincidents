<?php

require('../src/init.php');

use BugApp\Controllers\bugController;
use BugApp\Controllers\userController;

switch(true) {

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;
    
    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;

    case ($uri == 'login'):

        $controller = new userController();
            
        return $controller->login();
            
        break;
        
    case ($uri == 'logout'):

        $controller = new userController();
            
        return $controller->logout();
            
        break;
    
    default:
    
    http_response_code(404);
    
    echo "<h1>Gestion d'incidents</h1><p>Page par défaut</p>";
    //test git branche master
}