<?php

namespace BugApp\Controllers;

use BugApp\Models\UserManager;
use BugApp\Models\RecorderManager;
use BugApp\Models\Engineer;
use BugApp\Models\Recorder;
use BugApp\Controllers\abstractController;

class userController extends abstractController
{
    //Méthode Login
    public function login(){
        
        // Si il existe des données postées,
        if(!empty($_POST)){
        // vérifier que le login exite en base de données aux données saisies en base de données
        $email = $_POST['email'];
            //var_dump($email); //Pour vérifier si on a bien récupéré l'email

        $manager = new UserManager();

        $user = $manager -> findByEmail($email);

        //var_dump($user);
        //die;

        if($user !== null){
            // Si oui : Vérifier que le mot de passe correspond
            $password = $_POST['password'];
                //var_dump($password); //Pour vérifier si on a bien récupéré le password
            $check = $manager -> check($user, $password);

             // Si oui :
            if($check){
                
                switch(get_class($user)){

                    // Si l'utilisateur est un 'recorder', alors :
                    case 'BugApp\Models\Recorder':
                        // - créer une session
                        //session_start();
                        $_SESSION['user'] = serialize($user);
                        $_SESSION['type'] = 'recorder';
                        // - afficher la liste des incidents (vue Client)
                        header('Location:'.PUBLIC_PATH.'bug');
                    break;

                    // Si l'utilisateur est un 'ingénieur', alors :
                    case 'BugApp\Models\Engineer':
                        // - créer une session
                        //session_start();
                        $_SESSION['user'] = serialize($user);
                        $_SESSION['type'] = 'engineer';
                        // - afficher la liste des incidents (vue Ingenieur)
                        header('Location:'.PUBLIC_PATH.'bug');
                    break;
                }
            
            }else{
                // Si non (le mot de passe ne correspond pas) :
                $error = "Le mot de passe entré est incorrect.";
                // Il y a une erreur. Afficher le formulaire de login avec un commentaire
                $content = $this->render('src/Views/User/login', ['error' => $error]);

                return $this->sendHttpResponse($content, 200);
            }

        }else{
            // Si non (le login n'existe pas) :
            $error = "L'email entré est incorrect.";
             // Il y a une erreur. Afficher le formulaire de login avec un commentaire
            $content = $this->render('src/Views/User/login', ['error' => $error]);

            return $this->sendHttpResponse($content, 200);
        }

    }else{
        // Si non (pas de données postées
        // Afficher simplement le formulaire
        $content = $this->render('src/Views/User/login', []);

        return $this->sendHttpResponse($content, 200);
        }
    }
    

    //Méthode Logout
    public function logout(){
        session_destroy();
        unset($_SESSION);
        header('Location: '. PUBLIC_PATH .'login');
    }
}
