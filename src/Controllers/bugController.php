<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        //$bugs = [];
        // Données issues du Modèle

        $manager = new BugManager();

        $bugs = $manager->findAll();

        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/'.$_SESSION['type'].'/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident

        // TODO: ajout d'incident (GET et POST)

        $manager = new BugManager();
        if(isset($_POST['submit'])){

            $bug = new Bug();
            $bug->setTitle($_POST['title']);
            $bug->setDescription($_POST['description']);
            $bug->setCreatedAt($_POST['date'].'00:00:00');

            $manager->add($bug);
            //Pour le retour à la liste d'incidents
            header('Location: '.PUBLIC_PATH.'bug');
        }else{

            // Affiche le wireframe
            $content = $this->render('src/Views/Bug/Recorder/add', []);
            return $this->sendHttpResponse($content, 200);

        }

        
        
    }

}
