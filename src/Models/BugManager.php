<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug');
        $sth->execute();

        $bugs = [];
        // Instanciation d'un bug
        while ($result = $sth->fetch(\PDO::FETCH_ASSOC)) {
            $bug = new Bug();
            $bug->setId($result["id"]);
            $bug->setTitle($result["title"]);
            $bug->setDescription($result["description"]);
            $bug->setCreatedAt($result["createdAt"]);
            $bug->setClosedAt($result["closed"]);
            // Ajoute l'incident dans le tableau
            array_push($bugs, $bug);
        }
        // Retour
        return $bugs;
    }

    public function add(Bug $bug)
    {

        // Ajout d'un incident en BDD
        $bdd = static::connectDb();
        $req_insert=$bdd->prepare('insert into bug(title, description, createdAt)values(?,?,?)');
        $req_insert->execute([$bug->getTitle(), $bug->getDescription(), $bug->getCreatedAt()->format("Y-m-d H:i:s")]);
    }

    public function update(Bug $bug)
    {

        // Modification d'un incident en BDD
    }
}