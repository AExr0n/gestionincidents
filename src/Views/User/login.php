<!DOCTYPE html>

<html>

<?php if (isset($parameters['error'])) {
    $error = $parameters['error'];
} ?>

<head>
    <?php include('../src/Views/Include/header.php'); ?>
</head>

<body>
    <!-- NavBar -->
    <?php include('../src/Views/Include/nav.php'); ?>

    <!-- Formulaire de connexion -->
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center orange-text">Login</h1>
        </div>
    </div>

    <!-- Début container -->
    <div class="container">
        <div class="section">

            <div class="row"><?php if (isset($error)) echo $error; ?></div>

            <div class="row">
                <form class="col s12" method="post">
                    <div class="row center">
                        <div class="input-field col s12">
                            <input placeholder="Entrez votre email" id="email" type="email" class="validate" name="email">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row center">
                        <div class="input-field col s12">
                            <input placeholder="Entrez votre mot de passe" id="password" type="password" class="validate" name="password">
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row center">
                        <div class="col s12">
                            <button class="btn" type="submit" name="submit">Connexion
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <!-- Fin container -->
    </div>


    <br></br><br></br>

    <!-- Footer -->
    <?php include('../src/Views/Include/footer.php'); ?>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/js/materialize.js"></script>
    <script src="js/init.js"></script>
</body>

</html>