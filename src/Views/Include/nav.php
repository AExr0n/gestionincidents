<nav class="blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
        <a href="https://www.tarn.cci.fr/" target="_blank"> <img src="/img/CCI_tarn_logo.png" alt="Logo_page" title="Accueil" id="logo" width="130px" /> </a>
        <ul class="right hide-on-med-and-down">
            <?php
            if (isset($_SESSION['user'])) {

                $user = unserialize($_SESSION['user']);

            ?>
                <li>

                    <a href="#" class="user">

                        <img src="/img/user_logo.png" alt="Logo_user" class="circle responsive-img" width="35px" height="35px">
                        <span class="username"><?= $user->getNom(); ?></span>
                    </a>

                </li>

                <li>
                    <a href="logout" class="user">
                        <img src="/img/logo_logout.png" alt="Logo_logout" class="circle responsive-img" width="35px" height="35px">
                    </a>
                </li>

            <?php
            }
            ?>

            <!--<li><a href="#">Navbar Link</a></li>-->
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>
<div class="section no-pad-bot" id="index-banner">
    <br></br>