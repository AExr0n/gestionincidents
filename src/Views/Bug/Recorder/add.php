<!DOCTYPE html>

<html>

<head>
    <?php include('../src/Views/Include/header.php');?>
</head>

<body>
        <?php include('../src/Views/Include/nav.php');?>
        <!-- Début container (va contenir notre tableau, bouton, titres, etc...) -->
        <div class="container">

            <div class="row left">
                <div class="col s12">
                    <!-- Création d'un bouton -->
                    <form>
                        <a class="waves-effect waves-light btn" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>Retour à la liste</a>
                    </form>
                    <!-- Fin création d'un bouton -->
                </div>
            </div>

            <!-- Nom de la page -->
            <div class="row">
                <div class="col s12">
                    <h1 class="header center orange-text">Rapport incident</h1>
                </div>
            </div>

            <form method="POST">
                <div class="row">
                    <div class="input-field col s6">
                        <!--  Crée la zone pour entrer le nom de l'incident -->
                        <input id="titre_incident" type="text" class="validate" name='title'>
                        <label class="active" for="titre_incident">Nom de l'incident</label>
                        <!--  Fin de la zone -->
                    </div>
                    <div class="input-field col s6">
                        <!--  Crée la zone pour entrer la date de l'incident -->
                        <input type="date" class="validate" name='date'>
                        <label>Choisir la date</label>
                        <!--  Fin de la zone -->
                    </div>
                </div>

                <!--  Crée la zone pour la description de l'incident -->
                <div class="row">
                    
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="textarea1" class="materialize-textarea" name="description"></textarea>
                                <label class="active" for="textarea1">Tapez ici</label>
                            </div>
                        </div>
                    
                </div>
                <!--  Fin de la zone -->

                <!--  Crée un bouton "submit" pour ajouter le ticket -->
                <div class="row right">
                    <button class="btn waves-effect waves-light" type="submit" name="submit" >Ajouter
                        <i class="material-icons right">send</i>
                    </button>
                </div>
                <!-- Fin création d'un bouton -->

            </form>

        </div>
        <!-- Fin container (va contenir notre tableau, bouton, titres, etc...) -->

        <br></br><br></br>

        <?php include('../src/Views/Include/footer.php'); ?>

        <!--  Scripts-->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="/js/materialize.js"></script>
        <script src="/js/init.js"></script>
</body>

</html>