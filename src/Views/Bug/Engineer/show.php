<?php

/** @var $bug \BugApp\Models\Bug */

$bug = $parameters['bug'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php include('../src/Views/Include/header.php');?>
</head>

<body>
        <?php include('../src/Views/Include/nav.php');?>
        <!-- Début container (va contenir notre tableau, bouton, titres, etc...) -->
        <div class="container">

            <div class="row left">
                <div class="col s12">
                    <!-- Création d'un bouton -->
                    <form>
                        <a class="waves-effect waves-light btn" href="<?= PUBLIC_PATH; ?>bug"><i class="material-icons left">arrow_back</i>Retour à la liste</a>
                    </form>
                    <!-- Fin création d'un bouton -->
                </div>
            </div>

            <br></br><br></br>

            <h4 class="header orange-text">
                Fiche descriptive d'incidents
                <a href="update"><i class="icon-teal material-icons small">border_color</i></a>
            </h4>
            
            <br>
            <div class="row">
                <div class="col l3">
                    <h6 class="header black-text">Nom de l'incident :</h6>
                </div>
                <div class="col l9">
                    <h6 class="header black-text text-darken-2"> <?= $bug->getTitle(); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col l3">
                    <h6 class="header black-text">Date d'observation :</h6>
                </div>
                <div class="col l9">
                    <h6 class="header black-text text-darken-2"><?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></h6>
                </div>
            </div>

            <div class="row">
                <div class="col l3">
                    <h6 class="header black-text">Description de l'incident : </h6>
                </div>
                <div class="col l9">
                    <p class="black-text text-darken-2 justify"><?= $bug->getDescription(); ?></div>
            </div>
        </div>
        <!-- Fin container (va contenir notre tableau, bouton, titres, etc...) -->

        <br></br><br></br>

        <?php include('../src/Views/Include/footer.php'); ?>

        <!--  Scripts-->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="./materialize/js/materialize.js"></script>
        <script src="js/init.js"></script>
</body>

</html>