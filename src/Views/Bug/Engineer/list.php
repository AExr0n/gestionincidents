<?php

/** @var $bug \BugApp\Models\Bug */

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php include('../src/Views/Include/header.php'); ?>
</head>

<body>
    <?php include('../src/Views/Include/nav.php'); ?>
    <!-- Début container (va contenir notre tableau, bouton, titres, etc...) -->
    <div class="container">

        <!-- Nom de la page -->
        <div class="row">
            <div class="col s12">
                <h1 class="header center orange-text">Liste des incidents</h1>
            </div>
        </div>

        <br></br>

        <div class="row center">
            <!-- Création d'un tableau -->
            <table class=' highlight responsive-table'>
                <!-- "highlight = grise la ligne du tableau au survol de la souris / responsive-table = permet l'adaptabilité de l'affichage pour n'importe quel écran -->
                <!-- Nom des colonnes -->
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Sujet</th>
                        <th>Date</th>
                        <th>Utilisateur</th>
                        <th> </th>
                        <th>Ingénieur</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($bugs as $incident) {
                        echo '
                            <tr>
                                <td>' . $incident->getId() . '</td>
                                <td>' . $incident->getTitle() . '</td>
                                <td>' . $incident->getCreatedAt()->format("d/m/Y") . '</td>';
                        if ($incident->getClosedAt() != null) {
                            echo '<td>' . $incident->getClosedAt()->format("d/m/Y") . '</td>';
                        } else {
                            echo '<td></td>';
                        }
                        echo '
                                    <td><a href="' . PUBLIC_PATH . 'bug/show/' . $incident->getId() . '">Afficher</a></td>
                                    </tr>
                                ';
                    };
                    ?>
                </tbody>
            </table>
            <!-- Fin création d'un tableau -->
        </div>

    </div>
    <!-- Fin container (va contenir notre tableau, bouton, titres, etc...) -->

    <br></br><br></br>

    <?php include('../src/Views/Include/footer.php'); ?>

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../../bin/materialize.js"></script>
    <script src="js/init.js"></script>
</body>

</html>