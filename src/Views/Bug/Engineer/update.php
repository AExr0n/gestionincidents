<!DOCTYPE html>
<html lang="en">

<!-- Haut de page -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>AppGestionIncidents</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="./materialize/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection" />
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
</head>
<!-- Fin haut de page -->

<!-- Corps de page -->

<body style="background-color:rgba(245, 245, 245, 0.5);">
  <nav class="blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
    <a href="https://www.tarn.cci.fr/" target="_blank"> <img src="./CCI_tarn_logo.png" alt="Logo_page" title="Accueil" id="logo" width="130px"/> </a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <br></br>

    <!-- Début container (va contenir notre tableau, bouton, titres, etc...) -->
    <div class="container">

      <div class="row left">
        <div class="col s12">
          <!-- Création d'un bouton -->
          <form>
            <a class="waves-effect waves-light btn" href="./fiche_descrip_incident_inge.php"><i class="material-icons left">arrow_back</i>Retour à la fiche</a>
          </form>
          <!-- Fin création d'un bouton -->
        </div>
      </div>

      <br></br><br></br>

      <h4 class="header orange-text">Modification du rapport d'incidents</h4>
      <br>
      <div class="row">
        <div class="col l3">
          <h6 class="header black-text">Nom de l'incident : </h6>
        </div>
        <div class="col l9">
          <h6 class="header black-text text-darken-2">Erreur 404</h6>
        </div>
      </div>
      <div class="row">
        <div class="col l3">
          <h6 class="header black-text">Date d'observation :</h6>
        </div>
        <div class="col l9">
          <h6 class="header black-text text-darken-2">15/09/2020</h6>
        </div>
      </div>

      <div class="row">
        <div class="col l3">
          <h6 class="header black-text">Description de l'incident : </h6>
        </div>
        <div class="col l9">
          <p class="black-text text-darken-2 justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porttitor, mi eget venenatis suscipit, odio felis suscipit lacus, eget eleifend velit sapien dictum elit. Donec nec euismod massa. Integer dui sapien, sodales sed pretium nec, tempor a lectus. Nam et quam porta, rhoncus odio eget, dignissim lectus. Integer maximus lectus ut tellus gravida dignissim. Sed augue elit, congue in sodales at, dictum vestibulum diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sollicitudin tincidunt ligula, et lacinia diam vulputate et. Mauris nec justo rhoncus, vulputate sem quis, venenatis augue. Sed dapibus leo nec ligula consectetur, at fringilla sem volutpat. Nam blandit iaculis pulvinar. Nam mattis, diam non convallis vulputate, nisi lacus dapibus elit, ac maximus purus tellus vitae nibh. </p>
        </div>
      </div>

      <div class="row center">
        <div class="col l8 s6">
          <!-- Création d'un checkbox -->
          <p>
            <label>
              <input type="checkbox" />
              <span>Clôture de l'incident</span>
            </label>
          </p>
          <!-- Fin création d'un checkbox -->
        </div>
        <div class="col l4 s6">
          <!--  Crée un bouton "submit" pour ajouter le ticket -->
          <div class="row right">
            <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer
              <i class="material-icons right">send</i>
            </button>
          </div>
          <!-- Fin création d'un bouton -->
        </div>
      </div>

    </div>
    <!-- Fin container (va contenir notre tableau, bouton, titres, etc...) -->

    <br></br><br></br>

    <!-- Pieds de page -->
    <footer class="page-footer blue">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Company Bio</h5>
            <p class="grey-text text-lighten-4">coucou</p>
          </div>
          <div class="col l3 s12">
            <h5 class="white-text">Settings</h5>
            <ul>
              <li><a class="white-text" href="#!">Link 1</a></li>
              <li><a class="white-text" href="#!">Link 2</a></li>
              <li><a class="white-text" href="#!">Link 3</a></li>
              <li><a class="white-text" href="#!">Link 4</a></li>
            </ul>
          </div>
          <div class="col l3 s12">
            <h5 class="white-text">Connect</h5>
            <ul>
              <li><a class="white-text" href="#!">Link 1</a></li>
              <li><a class="white-text" href="#!">Link 2</a></li>
              <li><a class="white-text" href="#!">Link 3</a></li>
              <li><a class="white-text" href="#!">Link 4</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
          Made with <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a> by A.CABANAC
        </div>
      </div>
    </footer>
    <!-- Fin pieds de page -->

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="./materialize/js/materialize.js"></script>
    <script src="js/init.js"></script>

</body>

</html>